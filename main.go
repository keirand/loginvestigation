package main

import (
	"encoding/csv"
	"io"
	"log"
	"net"
	"os"

	"github.com/oschwald/geoip2-golang"
)

func main() {
	spycloudcsv, err := os.Open("spycloud.csv")
	if err != nil {
		panic(err)
	}

	spycloudreader := csv.NewReader(spycloudcsv)

	outputcsv, err := os.Create("Different_IP_Authentication.csv")
	if err != nil {
		panic(err)
	}

	header := []string{"Infected IP", "Infected IP Location", "Log IP", "Log IP Location", "Email", "Infected Time", "Log Time"}
	outputcsvwriter := csv.NewWriter(outputcsv)
	defer outputcsvwriter.Flush()

	outputcsvwriter.Write(header)

	for {
		spyrecord, err := spycloudreader.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			panic(err)
		}

		infectedIP := spyrecord[30]
		spyEmail := spyrecord[2]
		infectedTime := spyrecord[23]

		logscsv, err := os.Open("logs.csv")
		if err != nil {
			panic(err)
		}

		logsreader := csv.NewReader(logscsv)

		for {
			logrecord, err := logsreader.Read()
			if err == io.EOF {
				break
			}
			if err != nil {
				panic(err)
			}

			logEmail := logrecord[0]
			logIP := logrecord[5]
			timeCreated := logrecord[4]
			eventType := logrecord[2]

			if eventType == "2000" && spyEmail == logEmail && logIP != infectedIP {
				infectedIPLocation := getIPLocation(infectedIP)
				logIPLocation := getIPLocation(logIP)
				row := []string{infectedIP, infectedIPLocation, logIP, logIPLocation, spyEmail, infectedTime, timeCreated}
				outputcsvwriter.Write(row)
			}
		}

	}
}

func getIPLocation(ip string) string {
	db, err := geoip2.Open("~/Projects/GeoLite2-City_20200915/GeoLite2-City.mmdb")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	ipAddr := net.ParseIP(ip)
	if ipAddr == nil {
		return ""
	}
	record, err := db.Country(ipAddr)
	if err != nil {
		panic(err)
	}
	return record.Country.Names["en"]
}
